import React, { useState } from 'react'
import {
  Text, View, Image, TouchableOpacity
} from 'react-native'
import { observer } from 'startupjs'
import { BASE_URL } from '@env'
import './index.styl'

const randomText = {
  short: 'Sed lorem amet ipsum dolor et amet nullam consequat a feugiat consequat tempus veroeros sed consequat',
  middle: 'Donec imperdiet consequat consequat. Suspendisse feugiat congue posuere. Nulla massa urna, fermentum eget quam aliquet.',
  long: 'Nam elementum nisl et mi a commodo porttitor. Morbi sit amet nisl eu arcu faucibus hendrerit vel a risus. Nam a orci mi, elementum ac arcu sit amet, fermentum pellentesque et purus. Integer maximus varius lorem, sed convallis diam accumsan sed. Etiam porttitor placerat sapien, sed eleifend a enim pulvinar faucibus semper quis ut arcu. Ut non nisl a mollis est efficitur vestibulum. Integer eget purus nec nulla mattis et accumsan ut magna libero. Morbi auctor iaculis porttitor. Sed ut magna ac risus et hendrerit scelerisque. Praesent eleifend lacus in lectus aliquam porta. Cras eu ornare dui curabitur lacinia.'
}

const statistic = [
  { color: '#efa8b0', numbers: '5,120', name: 'Etiam', icon: '/icons/push-pin.png' },
  { color: '#c79cc8', numbers: '8,192', name: 'Magna', icon: '/icons/shield.png' },
  { color: '#a89cc8', numbers: '2,048', name: 'Tempus', icon: '/icons/gamepad.png' },
  { color: '#9bb2e1', numbers: '4,096', name: 'Aliquam', icon: '/icons/share.png' },
  { color: '#8cc9f0', numbers: '1,024', name: 'Nullam', icon: '/icons/apple.png' }
]

const headerButtons = ['Introduction', 'First Section', 'Second Section', 'Get Started']

export default observer(function Body() {
  const [active, setActive] = useState(0)

  return pug`
    View.bodyContainer
      View.body
        View.headerButtonsContainer
          each button,index in headerButtons
            TouchableOpacity.headerBtn(
              key=index
              styleName={'active': index===active}
              onPress=()=>setActive(index)
            )
              Text=button
        View.contentBlock(
          styleName='first'
        )
          View.imageContainerMain
            Image.image(
              source={ uri: BASE_URL + '/icons/pic01.jpg' }
              styleName='first'
            )
          View.contentContainer
            Text.mainTitle Ipsum sed adipiscing
            View.mainTitleBorder
            Text.content(
              styleName='first'
            )=randomText.short
            TouchableOpacity.btn
              Text Learn More
        View.contentBlock
          Text.mainTitle Magna veroeros
          View.mainTitleBorder
          View.contentBlockContainer
            each item,index in statistic
              if index <= 2
                View.magnaContainer(
                  key=index
                )
                  View.imageContainerMain
                    Image.image(
                      source={ uri: BASE_URL + item.icon }
                    )
                  Text.title Ipsum consequat
                  Text.firstSectionContent=randomText.short
          TouchableOpacity.btn
            Text Learn More
        View.contentBlock(
          styleName='middle'
        )
          Text.mainTitle Ipsum consequat
          View.mainTitleBorder
          Text.content=randomText.middle
        View.statisticBlock
          each item,index in statistic
            View.imageContainer(
              key=index
              styleName={ 'first':!index,'last':index === statistic.length-1 }
              style={ 'background-color': item.color }
            )
              Image.statisticImage(
                source={ uri: BASE_URL + item.icon }
              )
              Text.numbers= item.numbers
              Text.name= item.name
        View.statisticContent
          Text.longText=randomText.long
          TouchableOpacity.btn(
            styleName='statisticBtn'
          )
            Text Learn More
        View.contentBlock(
          styleName='last'
        )
          Text.mainTitle Congue imperdiet
          View.mainTitleBorder
          View.lastBlockContainer
            Text.content=randomText.middle
            View.btnGroup
              TouchableOpacity.btn(
                styleName='getStarted'
              )
                Text Get Started
              TouchableOpacity.btn
                Text Learn More
        `
})
