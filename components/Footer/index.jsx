import React from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import { observer } from 'startupjs'
import { BASE_URL } from '@env'
import './index.styl'

const icons = [
  '/icons/contacts/whatsapp.png',
  '/icons/contacts/reddit.png',
  '/icons/contacts/skype.png',
  '/icons/contacts/spotify.png',
  '/icons/contacts/twitter.png'
]

const contacts = [
  { name: 'Address', info: '1234 Somewhere Road • Nashville, TN 00000 • USA' },
  { name: 'Phone', info: '(000) 000-0000 x 0000' },
  { name: 'Email', info: 'information@untitled.tld' }
]

export default observer(function Footer() {
  return pug`
    View.footer
      View.footerContainer
        View.contentContainer
          View
            Text.title Aliquam sed mauris
            Text.content Sed lorem ipsum dolor sit amet et nullam consequat feugiat consequat magna adipiscing tempus etiam dolore veroeros. eget dapibus mauris. Cras aliquet, nisl ut viverra sollicitudin, ligula erat egestas velit, vitae tincidunt odio.
          View.btnContainer
            TouchableOpacity.btn
              Text Learn More
        View.contacts
          Text.title Etiam feugiat
          each contact,index in contacts
            View.contactsName(
              key=index
            )
              Text.block1=contact.name
              Text.block2=contact.info
          View.contactsIcons
            each icon,index in icons
              Image.contactsImage(
                key=index
                source={ uri: BASE_URL+ icon }
                styleName={first:!index}
              )
      Text.footerText © Untitled. Design: 
        TouchableOpacity.footerTextLink
          Text HTML5 UP
        Text . Demo Images: 
        TouchableOpacity.footerTextLink
          Text Unsplash.
    `
})
