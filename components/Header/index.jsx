import React from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import { observer } from 'startupjs'
import { BASE_URL } from '@env'
import './index.styl'

export default observer(function Header() {
  return pug`
    View.header
      Image.image(
        source={ uri: BASE_URL + '/icons/apple.png' }
      )
      Text.title Stellar
      Text.content Just another free, fully responsive site template built by 
        TouchableOpacity.headerTextLink
          Text @ajlkn 
        Text for 
        TouchableOpacity.headerTextLink
          Text HTML5 UP.
    `
})
